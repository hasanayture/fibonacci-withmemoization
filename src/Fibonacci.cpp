#include <iostream>
#include <map>
#include <functional>
#include <iomanip>

using TYPE = long long int;

TYPE fibonacci(TYPE index) {

	TYPE result { 0 };
	std::map<TYPE, TYPE> memoizeTable;
	bool isNegative { index < 0 };
	if (isNegative)
		index = -index;

	std::function<TYPE(TYPE index, std::map<TYPE, TYPE> &memoizeTable)> innerFibonacci;

	innerFibonacci = [&innerFibonacci](TYPE index, std::map<TYPE, TYPE> &memoizeTable) -> TYPE {

		if (index == 0)
			return 0;
		else if (index == 1)
			return 1;
		else if (index < memoizeTable.size())
			return memoizeTable[index];

		TYPE innerResult = innerFibonacci(index - 1, memoizeTable) + innerFibonacci(index - 2, memoizeTable);
		memoizeTable.insert( { index, innerResult });
		return innerResult;
	};

	result = innerFibonacci(index, memoizeTable);
	if (isNegative) {
		result = -result;
	}

	return result;

}
int main(int argc, char **argv) {

//	for (int i = 0; i < 10; i++) {
//		std::cout << std::setfill(' ') << std::setw(12) << fibonacci(-i) << std::endl;
//	}
//
//	for (int i = 0; i < 10; i++) {
//		std::cout << std::setfill(' ') << std::setw(12) << fibonacci(i) << std::endl;
//	}

	std::cout << "Type \"q\" to exit. Enter integer number to calculate Fibonacci \n";

	std::string input { "" };
	while (input != "q" && input != "Q") {
		std::cin >> input;
		try {
			int integerInput = std::stoi(input);
			std::cout << "Fibonacci( " << integerInput << " ) = " << fibonacci(integerInput) << std::endl;
		} catch (...) {
			std::cout << "Value is not valid\n";
		}
		std::cout << "Type \"q\" to exit. Enter integer number to calculate Fibonacci \n";
	}
	;

	std::cout << "Fibonacci MAIN END" << std::endl;
	return 0;
}

